/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author TEIN
 */
public class TestUserService {
    public static void main(String[] args) {
        System.out.println(UserService.gerUsers());
        UserService.addUser(new User("userx","password"));
        System.out.println(UserService.gerUsers());
        User updateUser = new User("usery" , "password");
        UserService.updateUser(3, updateUser);
        System.out.println(UserService.gerUsers());
        UserService.delUser(updateUser);
        System.out.println(UserService.gerUsers());
        UserService.delUser(1);
        System.out.println(UserService.gerUsers());
        System.out.println("");
    }
}
